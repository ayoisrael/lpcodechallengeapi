﻿using LPCodeChallenge.Context;
using LPCodeChallenge.Entities;
using LPCodeChallenge.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LPCodeChallenge.Repositories
{
    public class UserRepositoryAsync : GenericRepositoryAsync<User>, IUserRepositoryAsync
    {
        private readonly DbSet<User> _users;

        public UserRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _users = dbContext.Set<User>();
        }

        public IQueryable<User> GetAllUsersAsync()
        {
            var users = _users;
            return users;
        }

        public Task<User> GetUserByIdAsync(int userId)
        {
            var user = _users.Where(x => x.Id == userId).FirstOrDefaultAsync();
            return user;
        }

        public Task<User> GetUserByEmailAsync(string email)
        {
            var user = _users.Where(x => x.Email == email).FirstOrDefaultAsync();
            return user;
        }

        public Task<bool> UserExistAsync(string email)
        {
            return Task.Run(() => _users.Any(x => x.Email.ToLower() == email.ToLower()));
        }
    }
}
