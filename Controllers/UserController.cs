﻿using System.Threading.Tasks;
using LPCodeChallenge.Features.User.Command.AuthenticateUser;
using LPCodeChallenge.Features.User.Command.CreateUser;
using Microsoft.AspNetCore.Mvc;

namespace LPCodeChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseApiController
    {
        [HttpPost]
        public async Task<IActionResult> CreateUser(CreateUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateUser(AuthenticateUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

    }
}
