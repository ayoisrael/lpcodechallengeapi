﻿using AutoMapper;
using LPCodeChallenge.DTOs;
using LPCodeChallenge.Interfaces.Repositories;
using LPCodeChallenge.Interfaces.Services;
using LPCodeChallenge.Wrappers;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LPCodeChallenge.Features.User.Command.AuthenticateUser
{
    public class AuthenticateUserCommand : IRequest<Response<UserViewModel>>
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public class AuthenticateUserCommandHandler : IRequestHandler<AuthenticateUserCommand, Response<UserViewModel>>
        {
            private readonly IUserRepositoryAsync _userRepo;
            private readonly IEncryptionService _encryptionService;
            private readonly IMapper _mapper;

            public AuthenticateUserCommandHandler(IUserRepositoryAsync userRepo, IMapper mapper, IEncryptionService encryptionService)
            {
                _userRepo = userRepo;
                _mapper = mapper;
                _encryptionService = encryptionService;
            }

            public async Task<Response<UserViewModel>> Handle(AuthenticateUserCommand command, CancellationToken cancellationToken)
            {
                var isExist = await _userRepo.UserExistAsync(command.Email);

                if (!isExist) return new Response<UserViewModel>(null, "Login failed! Email does not exist.");

                var user = await _userRepo.GetUserByEmailAsync(command.Email);

                var decryptedPassword = _encryptionService.Decrypt(user.Password);

                if (decryptedPassword != command.Password) return new Response<UserViewModel>(null, "Login failed. Incorrect Password");

                var response = _mapper.Map<UserViewModel>(user);

                return new Response<UserViewModel>(response, "Login Successful!");
            }
        }
    }
}
