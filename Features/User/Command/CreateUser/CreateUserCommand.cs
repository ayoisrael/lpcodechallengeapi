﻿using AutoMapper;
using LPCodeChallenge.Interfaces.Repositories;
using LPCodeChallenge.Interfaces.Services;
using LPCodeChallenge.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LPCodeChallenge.Features.User.Command.CreateUser
{
    public class CreateUserCommand : IRequest<Response<int>>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }

        public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Response<int>>
        {
            private readonly IUserRepositoryAsync _userRepo;
            private readonly IEncryptionService _encryptionService;
            private readonly IMapper _mapper;

            public CreateUserCommandHandler(IUserRepositoryAsync userRepo, IMapper mapper, IEncryptionService encryptionService)
            {
                _userRepo = userRepo;
                _mapper = mapper;
                _encryptionService = encryptionService;
            }

            public async Task<Response<int>> Handle(CreateUserCommand command, CancellationToken cancellationToken)
            {
                var isExist = await _userRepo.UserExistAsync(command.Email);

                if (isExist) return new Response<int>(0, "User already exist.");

                var user = _mapper.Map<Entities.User>(command);

                user.Password = _encryptionService.Encrypt(command.Password);

                await _userRepo.AddAsync(user);

                return new Response<int>(user.Id, "User created successfully");
            }
        }
    }
}
