﻿using AutoMapper;
using LPCodeChallenge.DTOs;
using LPCodeChallenge.Entities;
using LPCodeChallenge.Features.User.Command.CreateUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LPCodeChallenge.Profiles
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<CreateUserCommand, User>();
            CreateMap<User, UserViewModel>();
        }
    }
}
