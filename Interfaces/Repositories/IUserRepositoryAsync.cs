﻿using LPCodeChallenge.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LPCodeChallenge.Interfaces.Repositories
{
    public interface IUserRepositoryAsync : IGenericRepositoryAsync<User>
    {
        Task<User> GetUserByIdAsync(int userId);
        IQueryable<User> GetAllUsersAsync();
        Task<bool> UserExistAsync(string email);
        Task<User> GetUserByEmailAsync(string email);
    }
}
