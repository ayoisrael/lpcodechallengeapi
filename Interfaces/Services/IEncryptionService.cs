﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LPCodeChallenge.Interfaces.Services
{
    public interface IEncryptionService
    {
        string Encrypt(string input);
        string Decrypt(string input);
        string Base64Encode(string plainText);
        string Base64Decode(string base64EncodedData);
    }
}
